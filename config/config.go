package config

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

// Config ...
type Config struct {
	Environment string // develop, staging, production

	PositionServiceHost string
	PositionServicePort int

	CompanyServiceHost string
	CompanyServicePort int

	LogLevel string
	HttpPort string

	ServiceName string
	JaegerHostPort string
}

// Load loads environment vars and inflates Config
func Load() Config {
	if err := godotenv.Load(); err != nil {
		fmt.Println("No .env file found")
	}

	config := Config{}

	config.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "develop"))

	config.LogLevel = cast.ToString(getOrReturnDefault("LOG_LEVEL", "debug"))
	config.HttpPort = cast.ToString(getOrReturnDefault("HTTP_PORT", ":8090"))

	config.PositionServiceHost = cast.ToString(getOrReturnDefault("POSITION_SERVICE_HOST", "localhost"))
	config.PositionServicePort = cast.ToInt(getOrReturnDefault("POSITION_SERVICE_PORT", 9102))
    
	config.CompanyServiceHost = cast.ToString(getOrReturnDefault("COMPANY_SERVICE_HOST", "localhost"))
	config.CompanyServicePort = cast.ToInt(getOrReturnDefault("COMPANY_SERVICE_PORT", 9103))

	config.ServiceName = cast.ToString(getOrReturnDefault("SERVICE_NAME", "go_api_gateway"))
	config.JaegerHostPort = cast.ToString(getOrReturnDefault("JAEGER_HOST_PORT", "localhost:6831"))


	return config
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}

	return defaultValue
}