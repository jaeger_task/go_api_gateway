package main

import (
	// "fmt"

	"fmt"
	"os"
	"os/signal"
	"syscall"

	"api_gateway/api"

	"api_gateway/config"
	"api_gateway/pkg/logger"
	"api_gateway/services"
	"github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-client-go"
	jaeger_config "github.com/uber/jaeger-client-go/config"
)

func main() {
	cfg := config.Load()
	log := logger.New(cfg.LogLevel, "api_gateway")

	jaegerCfg := &jaeger_config.Configuration{
		ServiceName: cfg.ServiceName,

		// "const" sampler is a binary sampling strategy: 0=never sample, 1=always sample.
		Sampler: &jaeger_config.SamplerConfig{
			Type:  "const",
			Param: 1,
		},

		// Log the emitted spans to stdout.
		Reporter: &jaeger_config.ReporterConfig{
			LogSpans:           true,
			LocalAgentHostPort: cfg.JaegerHostPort,
		},
	}
    tracer, closer, err := jaegerCfg.NewTracer(jaeger_config.Logger(jaeger.StdLogger))
	if err != nil {
		panic(fmt.Sprintf("ERROR: cannot init Jaeger: %v\n", err))
	}
	defer closer.Close()
	opentracing.SetGlobalTracer(tracer)

	
	gprcClients, _ := services.NewGrpcClients(&cfg)

	server := api.New(&api.RouterOptions{
		Log:      log,
		Cfg:      cfg,
		Services: gprcClients,
		Tracer: tracer,
	})

	quit := make(chan os.Signal, 1)
	go server.Run(cfg.HttpPort)

	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Info("Shutting down server...")

	log.Info("Server exiting")
}